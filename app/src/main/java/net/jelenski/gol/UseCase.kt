package net.jelenski.gol

interface UseCase<Event, State> {
    fun process(event: Event, oldState: State): State
}
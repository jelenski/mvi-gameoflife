package net.jelenski.gol

val <T> T.exhaustive: T get() = this
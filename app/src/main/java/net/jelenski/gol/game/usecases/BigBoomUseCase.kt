package net.jelenski.gol.game.usecases

import net.jelenski.gol.UseCase
import net.jelenski.gol.game.data.GameEvent
import net.jelenski.gol.game.data.GameState

class BigBoomUseCase : UseCase<GameEvent.BigBoom, GameState> {
    override fun process(event: GameEvent.BigBoom, oldState: GameState): GameState {
        return GameState(
            (0..400).map { true }
                .plus((401..1599).map { false })
                .shuffled()
        )
    }
}
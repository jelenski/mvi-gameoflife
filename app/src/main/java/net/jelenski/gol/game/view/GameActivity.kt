package net.jelenski.gol.game.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_main.*
import net.jelenski.gol.R
import net.jelenski.gol.game.vm.GameViewModel
import org.koin.androidx.scope.currentScope
import org.koin.androidx.viewmodel.ext.android.viewModel

class GameActivity : AppCompatActivity() {

    private val vm: GameViewModel by currentScope.viewModel(this)
    private val startedDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()
        vm.board
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                board.adapter = GameAdapter(it)
            }
            .addTo(startedDisposable)

        shuffle.setOnClickListener { vm.onShuffleClicked() }
    }

    override fun onStop() {
        startedDisposable.clear()
        super.onStop()
    }
}

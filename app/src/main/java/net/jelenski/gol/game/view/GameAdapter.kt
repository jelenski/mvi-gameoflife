package net.jelenski.gol.game.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.box.view.*
import net.jelenski.gol.R

class GameAdapter(private val items: List<Boolean>) : RecyclerView.Adapter<GameAdapter.BoxViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BoxViewHolder {
        val inflatedView = LayoutInflater.from(parent.context)
            .inflate(R.layout.box, parent, false)
        return BoxViewHolder(inflatedView)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: BoxViewHolder, position: Int) {
        holder.update(items[position])
    }

    class BoxViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun update(alive: Boolean) {
            itemView.box.setBackgroundResource(
                when {
                    alive -> android.R.color.black
                    else -> android.R.color.white
                }
            )
        }
    }
}
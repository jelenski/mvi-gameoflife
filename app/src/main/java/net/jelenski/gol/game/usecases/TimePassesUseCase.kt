package net.jelenski.gol.game.usecases

import net.jelenski.gol.UseCase
import net.jelenski.gol.game.data.GameEvent
import net.jelenski.gol.game.data.GameState
import kotlin.math.roundToInt
import kotlin.math.sqrt

class TimePassesUseCase : UseCase<GameEvent.TimePasses, GameState> {
    override fun process(event: GameEvent.TimePasses, oldState: GameState): GameState {
        val newBoard = oldState.board.mapIndexed { index, alive ->
            isCellGoingToBeAlive(oldState.board, index, alive)
        }
        return GameState(board = newBoard)
    }

    private fun isCellGoingToBeAlive(
        board: List<Boolean>,
        index: Int,
        alive: Boolean
    ): Boolean {
        // Rules:
        //   For a space that is 'populated':
        //      Each cell with one or no neighbors dies, as if by solitude.
        //      Each cell with four or more neighbors dies, as if by overpopulation.
        //      Each cell with two or three neighbors survives.
        //   For a space that is 'empty' or 'unpopulated'
        //      Each cell with three neighbors becomes populated.

        val liveNeighbours = board.countLiveNeighbours(index)

        return when {
            alive -> when {
                liveNeighbours <= 1 -> false
                liveNeighbours >= 4 -> false
                liveNeighbours == 2 || liveNeighbours == 3 -> true
                else -> true
            }
            else -> when (liveNeighbours) {
                3 -> true
                else -> false
            }
        }
    }

    private fun List<Boolean>.countLiveNeighbours(index: Int): Int {
        val boardSize = sqrt(size.toDouble()).roundToInt() // assuming board size is just a square of list size
        val position = index.toXy(boardSize)
        val (x, y) = position
        return ((x - 1)..(x + 1)).asSequence()
            .map { positionX ->
                ((y - 1)..(y + 1)).asSequence()
                    .map { positionY ->
                        positionX to positionY
                    }
            }
            .flatten()
            .filter { it != position }
            .count { (x, y) -> getOrNull(getIndex(x, y, boardSize)) == true }
    }

    private fun Int.toXy(boardSize: Int): Pair<Int, Int> = (this % boardSize) to (this / boardSize)

    private fun getIndex(x: Int, y: Int, boardSize: Int) = y * boardSize + x
}

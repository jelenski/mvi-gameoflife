package net.jelenski.gol.game.data

data class GameState(
    val board: List<Boolean> = emptyList()
)
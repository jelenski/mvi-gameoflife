package net.jelenski.gol.game.vm

import android.util.Log
import androidx.lifecycle.ViewModel
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import net.jelenski.gol.game.data.GameEvent
import net.jelenski.gol.game.data.GameState
import net.jelenski.gol.game.usecases.BigBoomUseCase
import net.jelenski.gol.game.usecases.ShuffleUseCase
import net.jelenski.gol.game.usecases.TimePassesUseCase
import java.util.concurrent.TimeUnit

class GameViewModel(
    private val bigBoomUseCase: BigBoomUseCase,
    private val timePassesUseCase: TimePassesUseCase,
    private val shuffleUseCase: ShuffleUseCase
) : ViewModel() {

    private val createdDisposable = CompositeDisposable()

    private val events = PublishRelay.create<GameEvent>()
    private val stateRelay = BehaviorRelay.create<GameState>()

    val board: Observable<List<Boolean>> = stateRelay.map { it.board }

    init {
        val initialState = GameState()
        events
            .serialize()
            .doOnEach { Log.d("M_J", "event: $it") }
            .scan(initialState) { oldState, event ->
                when (event) {
                    is GameEvent.BigBoom -> bigBoomUseCase.process(event, oldState)
                    is GameEvent.TimePasses -> timePassesUseCase.process(event, oldState)
                    is GameEvent.OnShuffleClicked -> shuffleUseCase.process(event, oldState)
                }
            }
            .doOnEach { Log.d("M_J", "state: $it") }
            .subscribe(stateRelay::accept)
            .addTo(createdDisposable)

        Observable.just(GameEvent.BigBoom)
            .subscribe(events::accept)
            .addTo(createdDisposable)

        Observable.just(Unit)
            .repeatWhen { completed -> completed.delay(1, TimeUnit.SECONDS) }
            .subscribeOn(Schedulers.computation())
            .map { GameEvent.TimePasses }
            .subscribe(events::accept)
            .addTo(createdDisposable)
    }

    fun onShuffleClicked() {
        events.accept(GameEvent.OnShuffleClicked)
    }

    override fun onCleared() {
        createdDisposable.clear()
        super.onCleared()
    }
}
package net.jelenski.gol.game.usecases

import net.jelenski.gol.UseCase
import net.jelenski.gol.game.data.GameEvent
import net.jelenski.gol.game.data.GameState

class ShuffleUseCase : UseCase<GameEvent.OnShuffleClicked, GameState> {
    override fun process(event: GameEvent.OnShuffleClicked, oldState: GameState): GameState {
        return GameState(oldState.board.shuffled())
    }
}
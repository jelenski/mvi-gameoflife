package net.jelenski.gol.game.data

sealed class GameEvent {
    object BigBoom : GameEvent()
    object TimePasses : GameEvent()
    object OnShuffleClicked : GameEvent()
}